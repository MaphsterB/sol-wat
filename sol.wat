(module
  ;; Debugging imports to print i32s in base 10 or 16.
  (import "console" "log" (func $logInt (param i32)))
  (import "console" "logHex" (func $logHex (param i32)))

  (; 
   ; memory is 2 64-byte buffers (front and back), each sized for the deck
   ; each deck is 54 cards = 52 regular cards + 2 jokers
   ; (64 bytes for 54 cards: the remaining waste is for (a) alignment, and (b) to allow at least
   ; 8 bytes between the buffers so an i64 write into one won't stomp the memory after it)
   ; non-jokers are 1 - 52, JA = 53, JB = 54
   ; one buffer is always the "front" buffer (the actual deck)
   ; the other buffer is the "back" buffer (scratch space used for shuffling and cutting)
   ; big shuffles will cause which is front and which is back to swap.
   ;)
  (memory (export "m") 1 1)
  (data (i32.const 0)
    "\01\02\03\04\05\06\07\08\09\0a\0b\0c\0d\0e\0f\10\11\12\13\14\15\16\17\18\19\1a\1b\1c\1d\1e\1f\20\21\22\23\24\25\26\27\28\29\2a\2b\2c\2d\2e\2f\30\31\32\33\34\35\36"
  )
  (; 40 bytes are reserved starting at 128 for the reverse lookup from string to card #, which is complicated.
   ; To convert the string to a number, we use the value + the suit modifier.
   ; Value is A 2 3 4 5 6 7 8 9 T J Q K --> 1 - 13
   ; Suit offset is 0 for C, 13 for D, 26 for H, 39 for S
   ; Those characters in UTF-8/ASCII comprise the values 50 ('2') - 84 ('T'), a range of 35 values.
   ; Because suit and value characters don't overlap, we can use the same table for both.
   ; Index in the table is simply character value - 50; what is stored is either the suit modifier or the value, as appropriate.
   ; (upper row = character; 'x' for don't care, lower row is hex value it maps to, '|' denotes 8-byte boundaries)
   ;   2  3  4  5  6  7  8  9| x  x  x  x  x  x  x  A| x  C  D  x  x  x  H  x| J  K  x  x  x  x  x  Q| x  S  T  x  x  x  x  x
   ;0x02 03 04 05 06 07 08 09|00 00 00 00 00 00 00 01|00 00 0d 00 00 00 1a 00|0b 0d 00 00 00 00 00 0c|00 27 0a 00 00 00 00 00
   ;)
  (data (i32.const 128)
    "\02\03\04\05\06\07\08\09\00\00\00\00\00\00\00\01\00\00\0d\00\00\00\1a\00\0b\0d\00\00\00\00\00\0c\00\27\0a\00\00\00\00\00"
  )
  ;; This is the forward lookup table from card # to string, a straightforward 108 bytes, with each card
  ;; as 2 1-byte ASCII/UTF-8 characters (we reserve 112 bytes to keep things aligned by multiples of 8)
  (data (i32.const 168)
    "AC2C3C4C5C6C7C8C9CTCJCQCKCAD2D3D4D5D6D7D8D9DTDJDQDKDAH2H3H4H5H6H7H8H9HTHJHQHKHAS2S3S4S5S6S7S8S9STSJSQSKSJAJB...."
  )
  ;; The remaining memory (280+) is free for clients to use for storing input/output strings.

  (global $ja (export "ja") (mut i32) (i32.const 52)) ;; location of joker A relative to $front
  (global $jb (export "jb") (mut i32) (i32.const 53)) ;; location of joker B relative to $front
  (global $front (export "front") (mut i32) (i32.const 0)) ;; offset to start of "front block"
  (global $back (export "back") (mut i32) (i32.const 64)) ;; offset to start of "back block"

  ;; moveJokerA - find JokerA; if on bottom put in slot 1, else move down 1
  ;; NOTE: *Might* cause deck buffers to swap, if JA is on bottom
  (func $moveJokerA (export "moveJokerA")
    (local $down1 i32)
    (i32.eq (get_global $ja) (i32.const 53))
    if
      ;; jokerA is on bottom
      ;; move everything down, then put jokerA in slot 1
      call $cascadeA
      (i32.store8 offset=1 (get_global $front) (i32.const 53))
      (set_global $ja (i32.const 1))
      ;; update jokerB position: $jb += 1, if $jb > 0
      (if (get_global $jb) (then
        (set_global $jb (i32.add (get_global $jb) (i32.const 1)))))
    else
      ;; normal case
      ;; get the card below jokerA; put it where the joker is
      (set_local $down1 (i32.add (get_global $ja) (i32.const 1)))
      (i32.store8
        (i32.add (get_global $front) (get_global $ja))
        (i32.load8_u (i32.add (get_global $front) (get_local $down1))))
      ;; put the joker where that card was
      (i32.store8 (i32.add (get_global $front) (get_local $down1)) (i32.const 53))
      ;; update jokerB position, if it was the card we just swapped with jokerA
      (if (i32.eq (get_global $jb) (get_local $down1)) (then
        (set_global $jb (get_global $ja))))
      ;; increment $ja
      (set_global $ja (get_local $down1))
    end
  )

  ;; moveJokerB - find JokerB; if bottom put in slot 2, if one up from that slot 1, else move down 2
  ;; NOTE: *Might* cause deck buffers to swap, if JB is 1 or 2 cards from bottom
  (func $moveJokerB (export "moveJokerB")
    (local $down1 i32)
    (local $down2 i32)
    (i32.eq (get_global $jb) (i32.const 53))
    if
      ;; joker is on bottom
      ;; move everything down, then put the joker in slot 2
      call $cascadeB2
      (i32.store8 offset=2 (get_global $front) (i32.const 54))
      (set_global $jb (i32.const 2))
      ;; update jokerA position: $ja += 1 if $ja > 1
      (if (i32.gt_u (get_global $ja) (i32.const 1)) (then
        (set_global $ja (i32.add (get_global $ja) (i32.const 1)))))
    else
      (i32.eq (get_global $jb) (i32.const 52))
      if
        ;; joker is 2nd from bottom
        ;; move everything down, then put the joker in slot 1
        call $cascadeB1
        (i32.store8 offset=1 (get_global $front) (i32.const 54))
        (set_global $jb (i32.const 1))
        ;; update jokerA position: $ja += 1, if 0 < $ja < 53
        (if (i32.and (get_global $ja) (i32.lt_u (get_global $ja) (i32.const 53))) (then
          (set_global $ja (i32.add (get_global $ja) (i32.const 1)))))
      else
        ;; normal case
        ;; slide the 2 cards below jokerB up, then place the joker in the gap
        (set_local $down1 (i32.add (get_global $jb) (i32.const 1)))
        (set_local $down2 (i32.add (get_global $jb) (i32.const 2)))
        (i32.store16
          (i32.add (get_global $front) (get_global $jb))
          (i32.load16_u (i32.add (get_global $front) (get_local $down1))))
        (i32.store8 (i32.add (get_global $front) (get_local $down2)) (i32.const 54))
        ;; update jokerA position if it was one of the 2 cards shifted
        (i32.eq (get_global $ja) (get_local $down1))
        (i32.eq (get_global $ja) (get_local $down2))
        i32.or
        if
          (set_global $ja (i32.sub (get_global $ja) (i32.const 1)))
        end
        ;; $jb += 2
        (set_global $jb (get_local $down2))
      end
    end
  )

  ;; cascadeA - joker A moving from bottom to top, causing all cards below the top to slide down one
  ;; We do this in 8-byte (i64) chunks, aligning as many *store* operations as possible
  ;; NOTE: Causes deck buffers to swap
  (func $cascadeA
    ;; the 1st store is misaligned to shift cards 1-7 down (this will overlap w/ the topmost store below)
    (i64.store offset=2 align=1 (get_global $back) (i64.load offset=1 (get_global $front)))
    ;; aligned moves in 8-byte chunks, bottom to top
    (i64.store offset=48 (get_global $back) (i64.load offset=47 (get_global $front)))
    call $ccommon
    ;; last, move the top card over directly
    (i32.store8 (get_global $back) (i32.load8_u (get_global $front)))
    call $swap ;; swap the buffers
  )

  ;; cascadeB2 - joker B moving from bottom to slot 2, causing all cards below the 2nd to slide down
  ;; NOTE: Causes deck buffers to swap
  (func $cascadeB2
    ;; the 1st store is misaligned to shift cards 2-7 down (this will overlap w/ the topmost store below)
    (i64.store offset=3 (get_global $back) (i64.load offset=2 align=1 (get_global $front)))
    ;; aligned moves in 8-byte chunks, bottom to top
    (i64.store offset=48 (get_global $back) (i64.load offset=47 (get_global $front)))
    call $ccommon
    ;; last, move the top 2 cards over directly
    (i32.store16 (get_global $back) (i32.load16_u (get_global $front)))
    call $swap ;; swap the buffers
  )

  ;; cascadeB1 - joker B moving from near-bottom to slot 1, causing all cards above 52 and below the top to slide down
  ;; NOTE: Causes deck buffers to swap
  (func $cascadeB1
    ;; one extra copy to keep 53 in place
    (i32.store8 offset=53 (get_global $back) (i32.load8_u offset=53 (get_global $front)))
    ;; the 1st store is misaligned to shift cards 1-7 down (this will overlap w/ the topmost store below)
    (i64.store offset=2 align=1 (get_global $back) (i64.load offset=1 (get_global $front)))
    ;; the 2nd store is also misaligned to shift cards 44-51 (this will overlap with the next store)
    (i64.store offset=45 (get_global $back) (i64.load offset=44 align=2 (get_global $front)))
    ;; aligned moves in 8-byte chunks, bottom to top (skipping the bottom-most in this version)
    call $ccommon
    ;; last, move the top card over directly
    (i32.store8 (get_global $back) (i32.load8_u (get_global $front)))
    call $swap ;; swap the buffers
  )

  ;; common chunk of aligned moves for all 3 versions of cascade()
  ;; handles all the aligned stores, bottom-to-top (the loads are still unavoidably byte-aligned)
  (func $ccommon
    (i64.store offset=40 (get_global $back) (i64.load offset=39 (get_global $front)))
    (i64.store offset=32 (get_global $back) (i64.load offset=31 (get_global $front)))
    (i64.store offset=24 (get_global $back) (i64.load offset=23 (get_global $front)))
    (i64.store offset=16 (get_global $back) (i64.load offset=15 (get_global $front)))
    (i64.store  offset=8 (get_global $back) (i64.load  offset=7 (get_global $front)))
  )

  ;; Flip the front and back buffers
  (func $swap
    ;; b/c front & back can only be either 0 or 64, we can flip just the one bit to achieve swapping
    (set_global $front (i32.xor (get_global $front) (i32.const 0x40)))
    (set_global $back (i32.xor (get_global $back) (i32.const 0x40)))
  )

  ;; tripleCut - split the deck into 3 piles delimited by jokers; swap the 1st and 3rd piles
  ;; i.e. (pile1 JA pile2 JB pile3) becomes (pile3 JA pile2 JB pile1)
  ;; NOTE: Causes deck buffers to swap
  (func $tripleCut (export "tripleCut")
    (local $j1 i32) ;; topmost joker's position
    (local $j2 i32) ;; other joker's position
    (local $btop i32) ;; whether A or B was topmost
    (local $i i32)
    (local $src i32)
    (local $dst i32)
    (tee_local $j1 (get_global $ja))
    (tee_local $j2 (get_global $jb))
    ;; swap if j1pos is not topmost
    i32.gt_u
    if
      get_local $j1
      get_local $j2
      set_local $j1
      set_local $j2
      (set_local $btop (i32.const 1))
    end
    ;; copy piles from front to back: pile3, then pile2, then pile1
    ;; pile3 is everything from ($j2 + 1) down to the bottom (53)
    ;; since we're copying from top to bottom, we don't care if we grab a few extra cards at the end.
    block
      loop
        ;; while ($src = $j2 + $i) < 53
        ;; (the extra +1 is moved into the offset=1 below)
        (tee_local $src (i32.add (get_local $j2) (get_local $i)))
        i32.const 52
        (br_if 1 (i32.gt_u))
        ;; copy a block of 8 cards
        (i64.store
          (i32.add (get_global $back) (get_local $i))
          (i64.load offset=1 (i32.add (get_global $front) (get_local $src))))
        ;; $i += 8
        (set_local $i (i32.add (get_local $i) (i32.const 8)))
        br 0
      end
    end
    ;; pile2 is everything from $j1 to $j2 inclusive
    (set_local $i (i32.const 0))
    (set_local $dst (i32.sub (i32.const 53) (get_local $j2)))
    block
      loop
        ;; while $src <= $j2
        (tee_local $src (i32.add (get_local $j1) (get_local $i)))
        get_local $j2
        (br_if 1 (i32.gt_u))
        ;; copy a block
        (i64.store
          (i32.add (i32.add (get_global $back) (get_local $dst)) (get_local $i))
          (i64.load (i32.add (get_global $front) (get_local $src))))
        ;; $i += 8
        (set_local $i (i32.add (get_local $i) (i32.const 8)))
        br 0
      end
    end
    ;; pile1 is the top of the deck (0) to ($j1 - 1)
    (set_local $dst (i32.add (i32.sub (i32.const 53) (get_local $j1)) (i32.const 1)))
    (set_local $i (i32.const 0))
    block
      loop
        ;; while $i < $j1
        (br_if 1 (i32.ge_u (get_local $i) (get_local $j1)))
        ;; copy a block
        (i64.store
          (i32.add (i32.add (get_global $back) (get_local $dst)) (get_local $i))
          (i64.load (i32.add (get_global $front) (get_local $i))))
        ;; $i += 8
        (set_local $i (i32.add (get_local $i) (i32.const 8)))
        br 0
      end
    end
    ;; determine new joker positions
    ;; j1' = (size of pile3) = 53 - j2
    ;; j2' = 53 - (size of pile1) = 53 - j1
    (set_global $ja (i32.sub (i32.const 53) (get_local $j2)))
    (set_global $jb (i32.sub (i32.const 53) (get_local $j1)))
    get_local $btop
    if
      get_global $ja
      get_global $jb
      set_global $ja
      set_global $jb
    end
    call $swap ;; swap the buffers
  )

  ;; countCut - given a cut position $c, remove the bottom card, cut at the position,
  ;;  swap the 2 piles created by the cut, then place the bottom card back on the bottom.
  ;;  if $c is 0, use the value of the bottom card as the cut position.
  ;; NOTE: Usually causes the blocks to swap (unless the cut card is a joker)
  (func $countCut (export "countCut") (param $c i32)
    (local $i i32)
    (local $sd i32) ;; short for src/dst
    (if (i32.eq (get_local $c) (i32.const 0)) (then
      (set_local $c (i32.load8_u offset=53 (get_global $front)))
    ))
    ;; early drop-out criteria: deck is unchanged if joker is on bottom
    (if (i32.ge_u (get_local $c) (i32.const 53)) (then (return)))
    ;; copy $c to bottom-1 from front to back in blocks of 8
    ;; note: implicit $i = 0 here.
    block
      loop
        ;; $sd = $c + $i (here $sd is used for $src)
        ;; while $sd < 53
        (set_local $sd (i32.add (get_local $c) (get_local $i)))
        (br_if 1 (i32.gt_u (get_local $sd) (i32.const 52)))
        ;; copy a block
        (i64.store
          (i32.add (get_global $back) (get_local $i))
          (i64.load (i32.add (get_global $front) (get_local $sd))))
        ;; $i += 8
        (set_local $i (i32.add (get_local $i) (i32.const 8)))
        br 0
      end
    end
    ;; copy from 0 to $c-1 from front to back in blocks of 8
    (set_local $i (i32.const 0))
    ;; how many cards should we have copied by now? 54 - 1 - $c; that's the starting dst
    (set_local $sd (i32.sub (i32.const 53) (get_local $c))) ;; (here $sd is used for $dst)
    block
      loop
        ;; while $i < $c
        (br_if 1 (i32.gt_u (get_local $i) (get_local $c)))
        ;; copy a block
        (i64.store
          (i32.add (i32.add (get_global $back (get_local $sd)) (get_local $i)))
          (i64.load (i32.add (get_global $front) (get_local $i))))
        ;; $i += 8
        (set_local $i (i32.add (get_local $i) (i32.const 8)))
        br 0
      end
    end
    ;; copy the bottom card
    (i32.store8 offset=53 (get_global $back) (i32.load8_u offset=53 (get_global $front)))
    ;; update joker positions
    (set_global $ja (call $ccUpdateJoker (get_global $ja) (get_local $c)))
    (set_global $jb (call $ccUpdateJoker (get_global $jb) (get_local $c)))
    ;; swap the blocks
    call $swap
  )

  ;; helper for $countCut: return joker's new position after cut
  ;; if joker was before cut, $j += (53 - $c)
  ;; if joker was after cut, $j -= $c
  ;; (can be optimized as $j += (X - $c), with X = either 53 or 0)
  (func $ccUpdateJoker (param $j i32) (param $c i32) (result i32)
    (select
      (i32.const 53)
      (i32.const 0)
      (i32.lt_u (get_local $j) (get_local $c)))
    get_local $c
    i32.sub
    get_local $j
    i32.add
  )

  ;; The output card determines the keystream character
  ;; To get it, go to deck[deck[0]]
  ;; This returns the card value, not the keystream value
  (func $getOutputCard (export "getOutputCard") (result i32)
    (local $c i32)
    (set_local $c (i32.load8_u offset=0 (get_global $front)))
    ;; JB = 54 in the deck, but if we find it here we change it to 53
    (i32.load8_u
      (i32.add
        (get_global $front)
        (select
          (i32.const 53)
          (get_local $c)
          (i32.gt_u (get_local $c) (i32.const 53)))))
  )

  ;; Generate a single keystream character (a value 1-52)
  ;; This "shuffles" the deck until the output card is not a joker
  (func $nextKey (export "nextKey") (result i32)
    (local $out i32)
    loop
      call $moveJokerA
      call $moveJokerB
      call $tripleCut
      (call $countCut (i32.const 0))
      (tee_local $out (call $getOutputCard))
      i32.const 52
      i32.gt_u
      br_if 0
    end
    get_local $out
  )

  ;; Encrypts or decrypts a UTF-8 string in memory given by (startAddr, length).
  ;; Will only modify alphabetic ASCII characters ([a-zA-Z]); other
  ;; characters will be skipped. Outputs will have the same case (uppercase/lowercase) as
  ;; inputs.
  ;; Error is raised if startAddr is less than 280.
  ;; Nothing is returned; the string is overwritten in-place.
  ;; $dec param is a bool: false to encrypt, true to decrypt
  (func $encDec (export "encDec") (param $start i32) (param $len i32) (param $dec i32)
    (local $i i32)
    (local $char i32)
    (local $off i32)
    (local $key i32)
    (if (i32.lt_u (get_local $start) (i32.const 280)) (then (unreachable)))
    (tee_local $i (get_local $start))
    (set_local $len (i32.add (;stack;) (get_local $len))) ;; len += start to become 'end'
    block
      loop
        ;; while $i < $len
        (br_if 1 (i32.ge_u (get_local $i) (get_local $len)))
        (tee_local $char (i32.load8_u (get_local $i)))
        (tee_local $off (call $asciiOffset (;stack;)))
        (if (i32.eqz (;stack;)) (then
          ;; non-alphabetical; keep scanning
          (set_local $i (i32.add (get_local $i) (i32.const 1)))
          (br 1)
        ))
        ;; We've got a valid input
        ;; Convert to the 0-25 range
        (set_local $char (i32.sub (get_local $char) (get_local $off)))
        (set_local $key (call $nextKey))
        ;; add or subtract $key depending on mode: encryption vs. decryption
        (set_local $char
          (select
            (i32.sub (get_local $char) (get_local $key))
            (i32.add (get_local $char) (get_local $key))
            (get_local $dec)))
        ;; Convert back to original case & store the result
        (i32.store8
          (get_local $i)
          (i32.add (i32.rem_s (get_local $char) (i32.const 26)) (get_local $off)))
        ;; increment and loop
        (set_local $i (i32.add (get_local $i) (i32.const 1)))
        br 0
      end
    end
  )

  ;; Helper - Given a UTF-8/ASCII byte, returns either:
  ;; 0, if the character is not ASCII alphabetical ([A-Za-z]), or...
  ;; the offset required to shift it into the range 0-25 for A-Z, otherwise
  ;; (this is 65 for uppercase letters, 97 for lowercase)
  (func $asciiOffset (param $byte i32) (result i32)
    (i32.and
      (i32.gt_u (get_local $byte) (i32.const 64))
      (i32.lt_u (get_local $byte) (i32.const 91)))
    if
      (return (i32.const 65))
    end
    (select
      (i32.const 97)
      (i32.const 0)
      (i32.and
        (i32.gt_u (get_local $byte) (i32.const 96))
        (i32.lt_u (get_local $byte) (i32.const 123))))
  )

  ;; Convenience function: calls encDec() in encrypt mode
  (func $encrypt (export "encrypt") (param $start i32) (param $len i32)
    (call $encDec (get_local $start) (get_local $len) (i32.const 0))
  )

  ;; Convenience function: calls encDec() in decrypt mode
  (func $decrypt (export "decrypt") (param $start i32) (param $len i32)
    (call $encDec (get_local $start) (get_local $len) (i32.const 0xffffffff)) ;; why set 1 bit when you can SET THEM ALL ;)
  )

  ;; setDeck - set the deck using a 54-byte array of Uint8's
  ;; Provide the index of the start of the array (length is implied)
  ;; each card must be specified exactly once; each card value must be
  ;; between 1 and 54 inclusive
  ;; start must be >= 280
  (func $setDeck (export "setDeck") (param $start i32)
    (local $tracker i64) ;; tracks which cards have been seen; one bit per card.
    (local $i i32)
    (local $card i32)
    (if (i32.lt_u (get_local $start) (i32.const 280)) (then (unreachable)))
    loop
      (set_local $card (i32.load8_u (i32.add (get_local $start) (get_local $i))))
      ;; check for JA/JB so we can mark their positions
      (if (i32.eq (get_local $card) (i32.const 53))
      (then
        (set_global $ja (get_local $i)))
      (else
        (if (i32.eq (get_local $card) (i32.const 54)) (then
          (set_global $jb (get_local $i))
        ))
      ))
      ;; mark the card as seen and store it
      (set_local $tracker (i64.or
        (i64.shl (i64.const 1) (i64.extend_u/i32 (get_local $card))) ;; 1-bit mask for the card
        (get_local $tracker)))
      (i32.store8 (get_local $i) (get_local $card))
      ;; loop check & increment
      (set_local $i (i32.add (get_local $i) (i32.const 1)))
      (br_if 0 (i32.lt_u (get_local $i) (i32.const 54)))
    end
    ;; If $tracker is exactly equal to 0x7ffffffffffffe (0's, 54 1's, 0)
    ;; then we have found each value from 1-54 exactly once.
    ;; Any other value, and the deck had duplicates or out-of-bound values.
    (if (i64.ne (get_local $tracker) (i64.const 0x7ffffffffffffe)) (then
      (unreachable)
    ))
    (set_global $front (i32.const 0))
    (set_global $back (i32.const 64))
  )

  ;; setDeckWithString - set the deck using a 108-character array
  ;; each card is 2 characters (value then suit)
  ;; only uppercase characters are accepted
  ;; card values are A 2 3 4 5 6 7 8 9 T J Q K
  ;; card suits are C D H S
  ;; e.g. Ten of Hearts is TH, 2 of Spades is 2S, etc.
  ;; jokers are JA and JB
  ;; provide the start index of the string in memory (must be == 168 or >= 280)
  ;; slower than setDeck().
  (func $setDeckWithString (export "setDeckWithString") (param $start i32)
    (local $tracker i64) ;; tracks which cards have been seen; one bit per card.
    (local $i i32)
    (local $str i32)
    (local $card i32)
    ;; 168 is allowed (for resetDeck()) otherwise $start must be >= 280
    (i32.and (i32.lt_u (get_local $start) (i32.const 280)) (i32.ne (get_local $start) (i32.const 168)))
    (if (;stack;) (then (unreachable)))
    loop
      ;; get the card string (2 chars): str = mem16[start + 2*i]
      (set_local $str (i32.load16_u
        (i32.add (get_local $start) (i32.shl (get_local $i) (i32.const 1)))))
      ;; TODO fail if chars are invalid
      (i32.eq (get_local $str) (i32.const 0x414a))
      if
        ;; special case if $str == "JA" (0x414a; remember endianness reverses the bytes)
        (set_global $ja (get_local $i))
        (set_local $card (i32.const 53))
      else
        (i32.eq (get_local $str) (i32.const 0x424a))
        if
          ;; special case if $str == "JB" (0x424a; ditto above)
          (set_global $jb (get_local $i))
          (set_local $card (i32.const 54))
        else
          ;; push suit modifier on the stack: val = mem8[128 + (2nd char) - 50]
          (i32.load8_u offset=128
            (i32.sub
              (i32.shr_u (i32.and (get_local $str) (i32.const 0xff00)) (i32.const 8)) ;; 2nd char is in the higher 8 bits
              (i32.const 50)))
          ;; push value on the stack: suit = mem8[128 + (1st char) - 50]
          (i32.load8_u offset=128
            (i32.sub
              (i32.and (get_local $str) (i32.const 0xff)) ;; 1st char is in the lower 8 bits
              (i32.const 50)))
          ;; card value = value + suit mod
          i32.add
          set_local $card
        end
      end
      ;; mark the card as seen and store it
      ;; (note: we can use i64.extend_i32_u now)
      (set_local $tracker (i64.or
        (i64.shl (i64.const 1) (i64.extend_u/i32 (get_local $card))) ;; 1-bit mask for the card
        (get_local $tracker)))
      ;; mark the card as seen and store it
      (set_local $tracker (i64.or
        (i64.shl (i64.const 1) (i64.extend_u/i32 (get_local $card))) ;; 1-bit mask for the card
        (get_local $tracker)))
      (i32.store8 (get_local $i) (get_local $card))
      ;; loop check & increment
      (set_local $i (i32.add (get_local $i) (i32.const 1)))
      (br_if 0 (i32.lt_u (get_local $i) (i32.const 54)))
    end
    ;; same check as in setDeck()
    (if (i64.ne (get_local $tracker) (i64.const 0x7ffffffffffffe)) (then
      (unreachable)
    ))
    (set_global $front (i32.const 0))
    (set_global $back (i32.const 64))
  )

  ;; convenience function: resets the deck to the default state
  (func $resetDeck (export "resetDeck")
    (call $setDeckWithString (i32.const 168))
  )

  ;; Set the deck by using a passphrase string.
  ;; Provide the string location (start, length) in memory; must be >= 280
  ;; The passphrase string will not be mutated.
  ;; Only ASCII letters (a-z, A-Z) will be used in the passphrase.
  ;; Other characters will simply be skipped and have no effect.
  ;; setJokers is a boolean: set to non-zero to use the last 2 passphrase characters
  ;; to set the positions of the jokers as the last step (the last 2 characters are
  ;; still used to shuffle as well)
  ;; Note: the deck will NOT be reset first
  (func $setDeckWithPassphrase (export "setDeckWithPassphrase") (param $start i32) (param $len i32) (param $setJokers i32)
    (local $i i32)
    (local $char i32)
    (local $off i32)
    (local $end i32)
    (local $jreg i64) ;; iff $setJokers, tracks last 2 valid passphrase characters.
    (if (i32.lt_u (get_local $start) (i32.const 280)) (then (unreachable)))
    (tee_local $i (get_local $start)) ;; could optimize away by just using $start
    (set_local $end (i32.add (;stack;) (get_local $len)))
    block
      loop
        (br_if 1 (i32.ge_u (get_local $i) (get_local $end)))
        (tee_local $char (i32.load8_u (get_local $i)))
        (tee_local $off (call $asciiOffset (;stack;)))
        ;; skip non-alphabetical characters
        (if (i32.eqz) (then
          (set_local $i (i32.add (get_local $i) (i32.const 1)))
          (br 1)
        ))
        ;; Normal shuffle...
        call $moveJokerA
        call $moveJokerB
        call $tripleCut
        (call $countCut (i32.const 0))
        ;; ...with an extra count cut
        ;; (no output card check; so no extra shuffle here for jokers? the spec isn't explicit; it says "instead of step 5")
        ;; (add 1 to char - offset; we want 1-26 for the count, not 0-25)
        (tee_local $char (i32.add (i32.sub (get_local $char) (get_local $off)) (i32.const 1)))
        call $countCut
        ;; Track the last 2 char values if $setJokers is enabled, so we don't have to
        ;; scan backwards and re-compute their values later.
        get_local $setJokers
        if
          ;; $jreg is just a 2-slot i32 shift register disguised as an i64
          (set_local $jreg (i64.or
            (i64.shl (get_local $jreg) (i64.const 32))
            (i64.extend_u/i32 (get_local $char))))
        end
        ;; increment and loop
        (set_local $i (i32.add (get_local $i) (i32.const 1)))
        br 0
      end
    end
    ;; If $setJokers is enabled, use the last 2 alphabetical characters to set the
    ;; JA/JB positions (this restricts their possible locations to indices 1-26)
    ;; If there aren't 2 alphabetical characters, error.
    get_local $setJokers
    if
      ;; We've got 2 valid chars if the left i32 of $jreg is non-zero (it should be 1-26).
      ;; (Using $i as a temp value here.)
      (tee_local $i (i32.wrap/i64 (i64.shr_u (get_local $jreg) (i64.const 32))))
      (if (;stack;) (then (unreachable)))
      get_local $i
      (i32.wrap/i64 (i64.and (get_local $jreg) (i64.const 0xffffffff)))
      call $setJokersFromPassphrase
    end
  )

  ;; Helper for setDeckWithPassphrase(). Removes the jokers from the
  ;; deck and re-inserts them at the 2 specified locations.
  (func $setJokersFromPassphrase (param $na i32) (param $nb i32)
    ;; Problem statement: we know...
    ;; 2 cards (jokers) are being removed, from anywhere in the deck.
    ;;  this leaves 2 gaps. if a gap remains, cards above it slide down.
    ;; 2 cards (jokers again) are being inserted, both somewhere in slots 1-26
    ;;  this forces cards after the insert to slide down, possibly filling a gap.
    ;; important: the insertion points and removal points can coincide.

    ;; Now, try to figure out how to do this in a single copy pass from $front to $back.

    ;; Current thought: keep track of the # of inserts and the # of removals 'left'
    ;; in the pass. We know $na, $nb (insertion) and $ja, $jb (removal) locations.
    ;; Each time we pass one, set $i back to it, adjust number of inserts and removals,
    ;; and keep going.
  )

  ;; Debugging function; writes a stringified deck to the 108 bytes starting at $start.
  ;; $start must be >= 280
  (func $printDeck (export "printDeck") (param $start i32)
    (local $i i32)
    (if (i32.lt_u (get_local $start) (i32.const 280)) (then (unreachable)))
    loop
      ;; $card = mem8u[$front + $i]
      ;; mem16[$start + 2 * $i] = mem16[128 + 2 * ($card - 1)]
      ;; note: 168 + 2(c - 1) = 126 + 2c
      (i32.store16
        (i32.add (get_local $start) (i32.shl (get_local $i) (i32.const 1)))
        (i32.load16_u offset=166
          (i32.shl
            (i32.load8_u (i32.add (get_global $front) (get_local $i)))
            (i32.const 1))))
      ;; while (++i < 54)
      (set_local $i (i32.add (get_local $i) (i32.const 1)))
      (br_if 0 (i32.lt_u (get_local $i) (i32.const 54)))
    end
  )

) ;; end of module
