const fs = require("fs");
const buf = fs.readFileSync("sol.wasm");

// I removed formatting newlines b/c node's REPL goes all tab-crazy
// with them in there during a .load, for some reason (might be my dev environment...)
const importObj = {console: {log: (arg) => console.log(arg), logHex: (arg) => console.log(arg.toString(16)),}};

const obj = await WebAssembly.instantiate(buf, importObj);
const ex = obj.instance.exports;
const mem = ex.m;

const enc = new TextEncoder("utf-8");
const dec = new TextDecoder("utf-8");

const vDeck1 = new DataView(mem.buffer, 0, 64);
const vDeck2 = new DataView(mem.buffer, 64, 64);

const vInput = new DataView(mem.buffer, 280);
const vPrinted = new DataView(mem.buffer, 800, 108);

function str() { ex.printDeck(800); return dec.decode(vPrinted).split(/(?=(?:..)*$)/).join(" "); }

function deck1() { return [...Array(54).keys()].map(i => vDeck1.getInt8(i)); }

function deck2() { return [...Array(54).keys()].map(i => vDeck2.getInt8(i)); }

function debug() { return [ str(), ex.ja.value, ex.jb.value, deck1(), deck2() ]; }

function debugShuffle() { str(); ex.moveJokerA(); str(); ex.moveJokerB(); str(); ex.tripleCut(); str(); ex.countCut(0); debug(); }

function _ed(str, i) { const _f = [ex.encrypt, ex.decrypt]; setInputString(str); _f[i](280, str.length); return dec.decode(new DataView(mem.buffer, 280, str.length)); }

function encrypt(str) { return _ed(str, 0); }

function decrypt(str) { return _ed(sr, 1); }

function setInputString(str) { const bytes = enc.encode(str); for (var i = 0; i < bytes.length; i++) vInput.setUint8(i, bytes[i]); }

function setInputArray(arr) { for (var i = 0; i < arr.length; i++) vInput.setUint8(i, arr[i]); }

// Some test data
const keyStream = [4, 49, 10, 24, 8, 51, 44, 6, 4, 33]; // Correct keystream outputs of a default deck
const testString = "AAAAA AAAAA"; // w/ default deck, should encrypt to "EXKYI ZSGEH"
const backwards = "JBJAKSQSJSTS9S8S7S6S5S4S3S2SASKHQHJHTH9H8H7H6H5H4H3H2HAHKDQDJDTD9D8D7D6D5D4D3D2DADKCQCJCTC9C8C7C6C5C4C3C2CAC";
const backwardsArr = [...Array(54).keys()].map(i => i+1).reverse();
